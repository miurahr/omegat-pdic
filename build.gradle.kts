import java.io.FileInputStream
import java.util.Properties
import org.gradle.crypto.checksum.Checksum

plugins {
    java
    checkstyle
    distribution
    signing
    id("org.gradle.crypto.checksum") version "1.4.0"
    id("org.omegat.gradle") version "1.5.11"
    id("com.github.spotbugs") version "6.0.18"
    id("com.diffplug.spotless") version "6.13.0"
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

// we handle cases without .git directory
val dotgit = project.file(".git")
var versionDescribe: String = ""
if (dotgit.exists()) {
    val exe = providers.exec {
        setIgnoreExitValue(true)
        setCommandLine("git", "describe", "--tags", "--always", "--first-parent", "--abbrev=7", "--match=v*", "HEAD")
    }
    versionDescribe = when {
        exe.result.get().exitValue.equals(0) -> exe.standardOutput.asText.get().trim()
        else -> "v0.1.0-SNAPSHOT"  // when git error
    }
} else {
    val gitArchival = project.file(".git-archival.properties")
    val props = Properties()
    props.load(FileInputStream(gitArchival))
    val desc = props.getProperty("describe").toString()
    versionDescribe = when {
        !desc.startsWith("\$Format") -> desc
        else -> "v0.1.0-SNAPSHOT"
    }
}
val cleanVersionRegex = "^v\\d+\\.\\d+(\\.\\d+)?$".toRegex()
val snapshotVersionRegex = "^v\\d+\\.\\d+.*-SNAPSHOT$".toRegex()
version = when {
    cleanVersionRegex.matches(versionDescribe) -> versionDescribe.substring(1)
    snapshotVersionRegex.matches(versionDescribe) -> versionDescribe
    else -> versionDescribe.substring(1) + "-SNAPSHOT"
}
tasks.register("printVersion") {
    doLast {
        println(project.version)
    }
    group = "verification"
}

val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find {project.hasProperty(it)}
tasks.withType<Sign> {
    onlyIf { !version.toString().endsWith("-SNAPSHOT") && (signKey != null) }
}

signing {
    when (signKey) {
        "signingKey" -> {
            val signingKey: String? by project
            val signingPassword: String? by project
            useInMemoryPgpKeys(signingKey, signingPassword)
        }
        "signing.keyId" -> {/* do nothing */}
        "signing.gnupg.keyName" -> {
            useGpgCmd()
        }
    }
    sign(tasks.distZip.get())
    sign(tasks.jar.get())
}

omegat {
    version = "6.0.0"
    pluginClass = "tokyo.northside.omegat.PDic"
    packIntoJarFileFilter = {it.exclude("META-INF/**/*", "module-info.class", "kotlin/**/*")}
}

repositories {
    mavenCentral()
}

dependencies {
    packIntoJar("tokyo.northside:pdic4j:0.6.0")
}

checkstyle {
    isIgnoreFailures = true
    toolVersion = "7.1"
}

distributions {
    main {
        contents {
            from(tasks["jar"], "README.md", "COPYING", "CHANGELOG.md")
        }
    }
}

val jar by tasks.getting(Jar::class) {
    duplicatesStrategy = DuplicatesStrategy.INCLUDE
}

spotless {
    format("misc") {
        target(listOf("*.gradle", ".gitignore", "*.rst"))
        trimTrailingWhitespace()
        indentWithSpaces()
        endWithNewline()
    }
    format("styling") {
        target(listOf("*.md"))
        prettier()
    }
    java {
        target(listOf("src/*/java/**/*.java"))
        removeUnusedImports()
        palantirJavaFormat()
        importOrder("org.omegat", "io.github.eb4j", "java", "javax", "", "\\#")
    }
}

tasks.distTar {
    isEnabled = false
}

tasks.register<Checksum>("createChecksums") {
    dependsOn(tasks.distZip)
    inputFiles.setFrom(listOf(tasks.jar.get(), tasks.distZip.get()))
    outputDirectory.set(layout.buildDirectory.dir("distributions"))
    checksumAlgorithm.set(Checksum.Algorithm.SHA512)
    appendFileNameToChecksum.set(true)
    group = "distribution"
}

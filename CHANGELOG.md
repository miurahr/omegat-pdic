# Change Log

All notable changes to this project will be documented in this file.

## [Unreleased]

## [v0.3.0]

- Bump required Java 11
- Bump versions
  - Gradle-omegat-plugin@1.5.9
  - Gradle@7.6
  - pdic4j@0.5.0

## [v0.2.1]

### Changed

- Bump versions
  - gradle-omegat-plugin@1.5.7
  - pdic4j@0.3.3

## [v0.2.0]

### Changed

- Use PDIC4j library for PDIC access
- Bump versions
  - spotbugs@5.0.4
  - spotless@6.1.2
  - actions/setup-java@2.5.0
- Test on Github actions

## v0.1.0

First imported version

[unreleased]: https://codeberg.org/miurahr/omegat-pdic/compare/v0.3.0...HEAD
[v0.3.0]: https://codeberg.org/miurahr/omegat-pdic/compare/v0.2.1...v0.3.0
[v0.2.1]: https://codeberg.org/miurahr/omegat-pdic/compare/v0.2.0...v0.2.1
[v0.2.0]: https://codeberg.org/miurahr/omegat-pdic/compare/v0.1.0...v0.2.0
